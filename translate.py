import pip

try:
    import pandas as pd
    import random
except ImportError:
    pip.main(["install", "pandas"])
    pip.main(["install", "random"])
    
xls = "Words.xlsx"
batch = 50

# load dictionary
xl = pd.ExcelFile(xls)

# load the words into dictionary
words = xl.parse('Sheet1').to_dict()
en_dic = words['EN']
ro_dic = words['RO']
mc_dic = words['MC']

max_id = len(en_dic.keys()) -1

for i in range(0, batch):
    crt_id = random.randint(0, max_id)

    en_word = en_dic[crt_id]
    ro_word = ro_dic[crt_id]
    mc_word = mc_dic[crt_id]

    search_type = random.randint(0, 1)
    if search_type == 0:
        if mc_word == '' or type(mc_word) is float:
            inp = raw_input('Tradu in limba engleza cuvantul "' + str(ro_word) + '": ')
        else:
            inp = raw_input('Tradu in limba engleza cuvantul "' + str(ro_word) + ' ' + str(mc_word) + '": ')
            
        if inp != en_word:
            print '\tRaspunsul corect este: "' + str(en_word) + '"'
    else:
        inp = raw_input('Tradu in limba romana cuvantul "' + str(en_word) + '": ')
        if inp != ro_word:
            print '\tRaspunsul corect este: "' + str(ro_word) + '"'